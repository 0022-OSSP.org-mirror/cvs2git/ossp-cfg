/*
**  OSSP cfg - Configuration Parsing
**  Copyright (c) 2002-2006 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2006 The OSSP Project (http://www.ossp.org/)
**
**  This file is part of OSSP cfg, a configuration parsing
**  library which can be found at http://www.ossp.org/pkg/lib/cfg/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  cfg_node.h: configuration nodes (internals)
*/

#ifndef __CFG_NODE_H__
#define __CFG_NODE_H__

#include "cfg.h"
#include "cfg_data.h"

struct cfg_node_st {
    /* attribute ownership */
    unsigned long   owner;   /* attribute ownership bit field */

    /* node linkage */
    cfg_node_t     *parent;  /* pointer to parent node */
    cfg_node_t     *rbroth;  /* pointer to right brother node */
    cfg_node_t     *child1;  /* pointer to first child node */

    /* node contents */
    cfg_node_type_t type;    /* type of node */
    char           *token;   /* pointer to corresponding token string */

    /* node annotation */
    cfg_data_t      data;    /* annotational data */

    /* node source location */
    char           *srcname; /* name of original source configuration */
    int             srcpos;  /* offset into original source configuration */
};

#endif /* __CFG_NODE_H__ */

