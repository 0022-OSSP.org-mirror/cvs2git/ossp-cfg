/*
**  OSSP cfg - Configuration Parsing
**  Copyright (c) 2002-2006 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2006 The OSSP Project (http://www.ossp.org/)
**
**  This file is part of OSSP cfg, a configuration parsing
**  library which can be found at http://www.ossp.org/pkg/lib/cfg/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  cfg_buf.h: auto-resizing string buffer
*/

#ifndef __CFG_BUF_H__
#define __CFG_BUF_H__

#include "cfg.h"

struct cfg_buf_st;
typedef struct cfg_buf_st cfg_buf_t;

extern cfg_rc_t cfg_buf_create  (cfg_buf_t **buf);
extern cfg_rc_t cfg_buf_resize  (cfg_buf_t  *buf, signed int n);
extern cfg_rc_t cfg_buf_append  (cfg_buf_t  *buf, const char *str, size_t len, char c);
extern cfg_rc_t cfg_buf_remove  (cfg_buf_t  *buf, const char *str, size_t len);
extern cfg_rc_t cfg_buf_format  (cfg_buf_t  *buf, const char *fmt, ...);
extern cfg_rc_t cfg_buf_vformat (cfg_buf_t  *buf, const char *fmt, va_list ap);
extern cfg_rc_t cfg_buf_content (cfg_buf_t  *buf, char **ptr, size_t *len, size_t *size);
extern cfg_rc_t cfg_buf_destroy (cfg_buf_t  *buf);

#endif /* __CFG_BUF_H__ */

