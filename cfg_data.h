/*
**  OSSP cfg - Configuration Parsing
**  Copyright (c) 2002-2006 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2006 The OSSP Project (http://www.ossp.org/)
**
**  This file is part of OSSP cfg, a configuration parsing
**  library which can be found at http://www.ossp.org/pkg/lib/cfg/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  cfg_data.h: configuration annotational data
*/

#ifndef __CFG_DATA_H__
#define __CFG_DATA_H__

#include "cfg.h"

/* configuration data value type */
typedef union {
    void  *p;
    char  *s;
    int    i;
    double f;
} cfg_data_value_t;

struct cfg_data_st {
    cfg_data_type_t    type;   /* data type    */
    cfg_data_value_t   value;  /* data value   */
    cfg_data_cb_t      ctrl;   /* data control */
};

extern cfg_rc_t  cfg_data_create       (cfg_data_t **data);
extern cfg_rc_t  cfg_data_init         (cfg_data_t  *data);

extern cfg_rc_t  cfg_data_destroy      (cfg_data_t  *data);
extern cfg_rc_t  cfg_data_kill         (cfg_data_t  *data);

extern cfg_rc_t  cfg_data_clone        (cfg_data_t  *data, cfg_data_t **clone);
extern cfg_rc_t  cfg_data_copy         (cfg_data_t  *data, cfg_data_t  *copy);

#endif /* __CFG_DATA_H__ */

