/*
**  OSSP cfg - Configuration Parsing
**  Copyright (c) 2002-2006 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2006 The OSSP Project (http://www.ossp.org/)
**
**  This file is part of OSSP cfg, a configuration parsing
**  library which can be found at http://www.ossp.org/pkg/lib/cfg/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  cfg_grid.h: grid memory allocator (declaration)
*/

#ifndef __CFG_GRID_H__
#define __CFG_GRID_H__

#include <stdlib.h>
#include <string.h>

#include "cfg.h"

struct cfg_grid_st;
typedef struct cfg_grid_st cfg_grid_t;

extern cfg_rc_t cfg_grid_create (cfg_grid_t **grid, size_t tile_size, int tile_num);
extern cfg_rc_t cfg_grid_alloc  (cfg_grid_t  *grid, void **tile);
extern cfg_rc_t cfg_grid_inside (cfg_grid_t  *grid, void  *tile);
extern cfg_rc_t cfg_grid_free   (cfg_grid_t  *grid, void  *tile);
extern cfg_rc_t cfg_grid_stat   (cfg_grid_t  *grid, int *chunks, int *bytes_mgmt, int *bytes_used, int *bytes_free, int *tiles_used, int *tiles_free);
extern cfg_rc_t cfg_grid_destroy(cfg_grid_t  *grid);

#endif /* __CFG_GRID_H__ */

