/*
**  OSSP cfg - Configuration Parsing
**  Copyright (c) 2002-2006 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2006 The OSSP Project (http://www.ossp.org/)
**
**  This file is part of OSSP cfg, a configuration parsing
**  library which can be found at http://www.ossp.org/pkg/lib/cfg/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  cfg_fmt.h: printf(3)-style formatting API
*/

#ifndef __CFG_FMT_H__
#define __CFG_FMT_H__

struct cfg_fmt_format_st {
    /* the output buffer */
    char *curpos;                        /* start of output buffer (first pos) */
    char *endpos;                        /* end   of output buffer (last pos)  */

    /* callback for flushing the output buffer */
    int (*flush)(
        struct cfg_fmt_format_st *spec   /* this cfg_fmt_format_t specification */
    );

    /* callback for formatting unknown %-constructs */
    void (*format)(
        struct cfg_fmt_format_st *spec,  /* this cfg_fmt_format_t specification */
        char *prefix_char,               /* output arg: prefix character */
        char *pad_char,                  /* output arg: padding character */
        char **s_buf,                    /* output arg: string buffer */
        size_t *s_len,                   /* output arg: string len */
        char *num_buf,                   /* input  arg: temporary buffer */
        int num_buf_size,                /* input  arg: temporary buffer len */
        char *extinfo,                   /* input  arg: extension information */
        char fmt_char,                   /* input  arg: current formatting character */
        va_list *ap                      /* in/out arg: variable argument pointer */
    );

    /* arbitrary passed-through application data */
    union {
        int i;
        long l;
        double d;
        void *vp;
    } data[6];
};

typedef struct cfg_fmt_format_st cfg_fmt_format_t;

int   cfg_fmt_format    (cfg_fmt_format_t *vbuff, const char *fmt, va_list ap);
int   cfg_fmt_vsprintf  (char *s, size_t n, const char *fmt, va_list ap);
char *cfg_fmt_vasprintf (const char *fmt, va_list ap);
int   cfg_fmt_sprintf   (char *s, size_t n, const char *fmt, ...);
char *cfg_fmt_asprintf  (const char *fmt, ...);

#endif /* __CFG_FMT_H__ */

