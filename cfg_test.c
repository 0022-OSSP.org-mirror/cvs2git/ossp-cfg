/*
**  OSSP cfg - Configuration Parsing
**  Copyright (c) 2002-2006 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2006 The OSSP Project (http://www.ossp.org/)
**
**  This file is part of OSSP cfg, a configuration parsing
**  library which can be found at http://www.ossp.org/pkg/lib/cfg/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  cfg_test.c: test suite
*/

/* standard system headers */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* OSSP cfg headers */
#include "cfg.h"
#include "cfg_util.h"
#include "cfg_global.h"

/* main test program procedure */
int main(int argc, char *argv[])
{
    cfg_rc_t rc;
    char *im_ptr;
    size_t im_size;
    size_t im_used;
    char *ex_ptr;
    char *error;
    cfg_t *cfg;
    cfg_node_t **vec;
    int i;

    /* command line processing */
    if (argc < 2 || argc > 3) {
        fprintf(stderr, "USAGE: %s <file> [<select>]\n", argv[0]);
        exit(1);
    }

    /* read configuration file into memory */
    if ((rc = cfg_util_readfile(argv[1], &im_ptr, &im_size, &im_used)) != CFG_OK) {
        fprintf(stderr, "ERROR: reading file \"%s\"\n", argv[1]);
        exit(1);
    }

    /* create configuration object */
    if ((rc = cfg_create(&cfg)) != CFG_OK) {
        cfg_error(cfg, rc, &error);
        fprintf(stderr, "ERROR: cfg_create: %s\n", error);
        free(im_ptr);
        exit(1);
    }

    /* parse configuration */
    if ((rc = cfg_import(cfg, NULL, CFG_FMT_CFG, im_ptr, im_used)) != CFG_OK) {
        cfg_error(cfg, rc, &error);
        fprintf(stderr, "ERROR: cfg_import: %s\n", error);
        free(im_ptr);
        cfg_destroy(cfg);
        exit(1);
    }

    /* free configuration file in memory */
    free(im_ptr);

    /* export configuration again into memory */
    if ((rc = cfg_export(cfg, NULL, CFG_FMT_CFG, &ex_ptr, 0)) != CFG_OK) {
        cfg_error(cfg, rc, &error);
        fprintf(stderr, "ERROR: cfg_export: %s\n", error);
        cfg_destroy(cfg);
        exit(1);
    }

    /* print results */
    fprintf(stdout, "%s", ex_ptr);
    fflush(stdout);

    /* free configuration file in memory */
    free(ex_ptr);

    /* optional node selection */
    if (argc == 3) {
        fprintf(stdout, "==== selection process ====\n");
        if ((rc = cfg_node_select(cfg, NULL, &vec, "%s", argv[2])) != CFG_OK) {
            cfg_error(cfg, rc, &error);
            fprintf(stderr, "ERROR: cfg_node_select: %s\n", error);
            cfg_destroy(cfg);
            exit(1);
        }
        fprintf(stdout, "==== selection results ====\n");
        for (i = 0; vec[i] != NULL; i++) {
            fprintf(stdout, "---- selection result #%d ----\n", i);
            if ((rc = cfg_export(cfg, vec[i], CFG_FMT_CFG, &ex_ptr, 0)) != CFG_OK) {
                cfg_error(cfg, rc, &error);
                fprintf(stderr, "ERROR: cfg_export: %s\n", error);
                cfg_destroy(cfg);
                exit(1);
            }
            fprintf(stdout, "%s", ex_ptr);
            fprintf(stdout, "---- selection #%d ----\n", i);
            free(ex_ptr);
        }
        fprintf(stdout, "==== selection end ====\n");
    }

    /* destroy configuration object */
    cfg_destroy(cfg);

    return 0;
}

