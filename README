   _        ___  ____ ____  ____          __
  |_|_ _   / _ \/ ___/ ___||  _ \    ___ / _| __ _
  _|_||_| | | | \___ \___ \| |_) |  / __| |_ / _` |
 |_||_|_| | |_| |___) |__) |  __/  | (__|  _| (_| |
  |_|_|_|  \___/|____/____/|_|      \___|_|  \__, |
                                             |___/
  OSSP cfg - Configuration Parsing
  Version 0.9.11 (10-Aug-2006)

  ABSTRACT

  OSSP cfg is a ISO-C library for parsing arbitrary C/C++-style
  configuration files. A configuration is sequence of directives. Each
  directive consists of zero or more tokens. Each token can be either
  a string or again a complete sequence. This means the configuration
  syntax has a recursive structure and this way allows to create
  configurations with arbitrarily nested sections.

  Additionally the configuration syntax provides complex
  single/double/balanced quoting of tokens, hexadecimal/octal/decimal
  character encodings, character escaping, C/C++ and Shell-style
  comments, etc. The library API allows importing a configuration text
  into an Abstract Syntax Tree (AST), traversing the AST and optionally
  exporting the AST again as a configuration text.

  COPYRIGHT AND LICENSE

  Copyright (c) 2002-2006 Ralf S. Engelschall <rse@engelschall.com>
  Copyright (c) 2002-2006 The OSSP Project <http://www.ossp.org/>

  This file is part of OSSP cfg, a configuration parsing library which
  can be found at http://www.ossp.org/pkg/lib/cfg/.

  Permission to use, copy, modify, and distribute this software for
  any purpose with or without fee is hereby granted, provided that
  the above copyright notice and this permission notice appear in all
  copies.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.

  INTERNET LOCATIONS

  Homepage:     http://www.ossp.org/pkg/lib/cfg/
  Repository:   http://cvs.ossp.org/pkg/lib/cfg/
  Distribution:  ftp://ftp.ossp.org/pkg/lib/cfg/

