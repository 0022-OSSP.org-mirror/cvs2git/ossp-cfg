##
##  OSSP cfg - Configuration Parsing
##  Copyright (c) 2002-2006 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2002-2006 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP cfg, a configuration parsing library which
##  can be found at http://www.ossp.org/pkg/lib/cfg/.
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  cfg.tm: Perl XS typemap for xsubpp(1)
##

TYPEMAP
cfg_rc_t        T_IV
cfg_fmt_t       T_IV
cfg_node_type_t T_IV
cfg_node_attr_t T_IV
cfg_data_type_t T_IV
cfg_data_ctrl_t T_IV
cfg_data_attr_t T_IV
cfg_t *         T_PTRREF
cfg_t **        T_PTRREF
cfg_node_t *    T_PTRREF
cfg_node_t **   T_PTRREF
cfg_node_t ***  T_PTRREF
cfg_data_t *    T_PTRREF
cfg_data_cb_t   T_PTRREF
char **         T_PTRREF

