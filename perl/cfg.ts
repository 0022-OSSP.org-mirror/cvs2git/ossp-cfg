##
##  OSSP cfg - Configuration Parsing
##  Copyright (c) 2002-2006 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2002-2006 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP cfg, a configuration parsing library which
##  can be found at http://www.ossp.org/pkg/lib/cfg/.
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  cfg.ts: Perl Binding (Perl test suite part)
##

use Test::More tests => 8;

##
##  Module Loading
##

use IO::File;

BEGIN {
    use_ok('OSSP::cfg');
};
BEGIN {
    use OSSP::cfg qw(:all);
    ok(defined(CFG_VERSION), "CFG_VERSION");
    ok(CFG_OK == 0, "CFG_OK");
};

##
##  C-Style API
##

my ($rc, $cfg);

$rc = cfg_create($cfg);
ok($rc == CFG_OK, "cfg_create");

my ($in_ptr, $in_len);
my $io = new IO::File "../cfg_test.cfg" or die;
my $str; { local $/; $str = <$io>; }
$io->close();
$rc = cfg_import($cfg, \0, CFG_FMT_CFG, $str, length($str));
ok($rc == CFG_OK, "cfg_import");

my $out;
$rc = cfg_export($cfg, \0, CFG_FMT_CFG, $out, 0);
ok($rc == CFG_OK, "cfg_export");

$rc = cfg_destroy($cfg);
ok($rc == CFG_OK, "cfg_destroy");

##
##  OO-style API
##

$cfg = new OSSP::cfg;
ok(defined($cfg), "new OSSP::cfg");

undef $cfg;

