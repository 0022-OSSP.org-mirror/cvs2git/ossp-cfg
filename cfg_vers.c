/*
**  cfg_vers.c -- Version Information for OSSP cfg (syntax: C/C++)
**  [automatically generated and maintained by GNU shtool]
*/

#ifdef _CFG_VERS_C_AS_HEADER_

#ifndef _CFG_VERS_C_
#define _CFG_VERS_C_

#define __CFG_VERSION 0x00920B

typedef struct {
    const int   v_hex;
    const char *v_short;
    const char *v_long;
    const char *v_tex;
    const char *v_gnu;
    const char *v_web;
    const char *v_sccs;
    const char *v_rcs;
} __cfg_version_t;

extern __cfg_version_t __cfg_version;

#endif /* _CFG_VERS_C_ */

#else /* _CFG_VERS_C_AS_HEADER_ */

#define _CFG_VERS_C_AS_HEADER_
#include "cfg_vers.c"
#undef  _CFG_VERS_C_AS_HEADER_

__cfg_version_t __cfg_version = {
    0x00920B,
    "0.9.11",
    "0.9.11 (10-Aug-2006)",
    "This is OSSP cfg, Version 0.9.11 (10-Aug-2006)",
    "OSSP cfg 0.9.11 (10-Aug-2006)",
    "OSSP cfg/0.9.11",
    "@(#)OSSP cfg 0.9.11 (10-Aug-2006)",
    "$Id: cfg_vers.c,v 1.17 2006/08/10 19:32:57 rse Exp $"
};

#endif /* _CFG_VERS_C_AS_HEADER_ */

