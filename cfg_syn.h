/*
**  OSSP cfg - Configuration Parsing
**  Copyright (c) 2002-2006 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2006 The OSSP Project (http://www.ossp.org/)
**
**  This file is part of OSSP cfg, a configuration parsing
**  library which can be found at http://www.ossp.org/pkg/lib/cfg/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  cfg_syn.h: syntax parsing and formatting header
*/

#ifndef __CFG_SYN_H__
#define __CFG_SYN_H__

#include <string.h>
#include <stdarg.h>

#include "cfg.h"
#include "cfg_grid.h"
#include "cfg_node.h"
#include "cfg_buf.h"

/* internal specification scanner/parser context */
typedef struct {
    const char   *inputptr;   /* input buffer: current reading pointer */
    const char   *inputbuf;   /* input buffer: begin of buffer */
    size_t        inputlen;   /* input buffer: size  of buffer */
    cfg_t        *cfg;        /* the configuration object */
    cfg_node_t   *node;       /* top-level/root/result channel */
    cfg_rc_t      rv;         /* return value */
    cfg_buf_t    *buf;        /* temporary buffer */
    char         *err_buf;    /* error buffer pointer */
    size_t        err_len;    /* error buffer length */
    void         *yyscan;     /* Flex scanner context */
} cfg_syn_ctx_t;

/* internal scanner/parser token location */
typedef struct {
    int first;
    int last;
} cfg_syn_loc_t;
#define YYLTYPE cfg_syn_loc_t

/* support for automatic location tracking by Bison */
#define first_line   first
#define first_column first
#define last_line    last
#define last_column  last

/* internal API */
extern cfg_rc_t cfg_syn_import  (cfg_t *cfg, cfg_node_t **node, const char *in_ptr, size_t in_len, char *err_buf, size_t err_len);
extern cfg_rc_t cfg_syn_export  (cfg_t *cfg, cfg_node_t *node, char **output);
extern cfg_rc_t cfg_syn_destroy (cfg_t *cfg, cfg_node_t *node);

/* error reporting function */
extern void cfg_syn_error(cfg_syn_ctx_t *ctx, cfg_rc_t rv, YYLTYPE *loc, const char *fmt, ...);

#endif /* __CFG_SYN_H__ */

